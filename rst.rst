.. inline

this is a paragraph to test inline markup.
this is a *text* for emphasis.
this is a **text** for strong emphasis.
this is a ``inline code``.

.. lists

1. list 1
2. list 2

* list a
* list b

    * b1
    * b2

* list c

.. definition

term
    Definition of term.

    can contain multiple lines.

.. quote

ref a quote::
    
    quote paragraph

.. line blocks

| These lines are
| broken exactly like in 
| the source file

.. simple code

simple python code

::
    
    def hello_world():
        print "hello world"

>>> 1 + 1
2
>>>

c code goes here

.. code-block:: c
    :linenos:
    :emphasize-lines: 2,4
    
    int main() {
        printf("hello world!");

        return 0;
    }

.. href

.. _a link: http://chouqin.github.com

.. header

Header 1
##############

Header 2
**************

Header 3
==============

Header 4
--------------

Header 5
^^^^^^^^^^^^^^

Header 6
""""""""""""""

.. attention

.. note::

    this is a note

.. error::

    this is a error

.. hint::

    this is a hint

.. cross ref

.. ref to :ref:`Header 2 <h2>`.

.. math

.. math::
    
    (a + b)^2 = a^2 + 2ab + b^2
    
    \alpha + \theta = x

