Notes of Cubes
+++++++++++++++

Source Code
=============

create table from csv
----------------------

::
    
    __iter__

    csv.reader

    codecs.getreader

    row = self.reader.next()
    return [unicode(s, "utf-8") for s in row]

