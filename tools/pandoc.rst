利用Pandoc写文档并转换成pdf
===============================

其实Pandoc就是用markdown来写文档，
但是由于对中文的支持不是很好，
所以需要自己提供模板，而且需要指定默认的引擎。

::

    pandoc -o solution.pdf solution.md --latex-engine=xelatex\
    --template=mytemplate.tex

我根据自己的需要自定义了一个模板，作为gist保存起来。

**永远需要记住使用安装好的字体。**

Adobe的 ``Source Code Pro`` 真的很漂亮。

另外就是生成list或者代码之后不会自动换行，再想个办法去解决。
（可以通过在前面加一个空的Header去解决），
所以让pandoc输出空行都可以通过这种方式，而且可以有多个###############。
