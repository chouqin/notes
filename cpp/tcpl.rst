.. highlight:: c

The C Programming Language
===========================

Questions
---------------------------

How to read a variable?
^^^^^^^^^^^^^^^^^^^^^^^^^^^
Using ``scanf`` family.

::

    char str [80];
    int i;

    printf ("Enter your family name: ");
    scanf ("%s",str);  
    printf ("Enter your age: ");
    scanf ("%d",&i);


How to calculate ``sizeof`` ?
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

1. int, long, float, unsigned int, point: 4 bytes.
   (unsigned would not change the size)

2. double 8 bytes.

3. short 2 bytes.

4. array: array length * single size. 
   note the difference with pointer. 

5. struct 4 bytes alignment.

6. bool(C++ only) 1 byte.


Chapter 1
---------------------------


Chapter 5
---------------------------

A pointer is a variable, so ``pa=a`` and ``pa++`` are legal. But an array name is not a variable;
constructions like ``a=pa`` and ``a++`` are illegal.

::

    char str[20];
    str = "hello world!"; /* error */

It is possible to pass part of an array to a function, by passing a pointer to the beginning of the
subarray. For example, ``f(a+2)`` .

The valid pointer operations are assignment of pointers of the same type, adding or
subtracting a pointer and an integer, subtracting or comparing two pointers to members of the
same array, and assigning or comparing to zero. 

Note the difference.

::

    char amessage[] = "now is the time"; /* an array */
    char *pmessage = "now is the time"; /* a pointer */

    pmessage[0] = 'N'; /* segment fault */ 

Array of Pointers: ``char *lineptr[MAXLINES]``, 
mind the difference with ``int (*daytab)[13]`` or ``int daytab[][13]``, 
the latter is an array of array which the column number is 13.

Pointer to Pointer(Array of Pointers) differ with multi-dimensional array
in that the latter has to be equal length of each dimension.

    The important advantage of the pointer array is that the rows of the
    array may be of different lengths.

Function Pointer:

::

    void qsort(void *lineptr[], int left, int right,
        int (*comp)(void *, void *)); /* function to use function pointer*/

    qsort((void**) lineptr, 0, nlines-1,
        (int (*)(void*,void*))(numeric ? numcmp : strcmp));
    /* call this function using real function pointer, 
    ** note that numcmp and strcmp are just function names;
    ** also &numcmp and &strcmp
    */

    if ((*comp)(v[i], v[left]) < 0)... 
    /* using function pointer in qsort 
    ** which is is consistent with the declaration
    ** comp is also accepted
    */


Chapter 6
----------------------------

Structures are called ``records`` in some languages.

Structure assignment is a very important topic.

A style to define long functions.

::
    
    struct key *
    binsearch(char *word, struct key *tab, int n)

use this to new a struct ``return (struct tnode *) malloc(sizeof(struct tnode));``.

::

    p = (struct tnode *) malloc(sizeof(struct tnode));
    if (p != NULL) ... /* always check after malloc */

The strlen() function calculates the length of the string s, excluding the terminating null byte ('\\0').

The for loop in lookup is the standard idiom for walking along a linked list:

::

    for (ptr = head; ptr != NULL; ptr = ptr->next)

Define a function point type: ``typedef int (*PFI)(char *, char *);``.

A union may only be initialized with a value of the type of its first member; thus union u
described above can only be initialized with an integer value.


