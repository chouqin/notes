.. notes documentation master file, created by
   sphinx-quickstart on Tue Jan 29 12:36:55 2013.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

.. raw:: latex

    \renewcommand\partname{部分}
    \renewcommand{\chaptermark}[1]{\markboth{第 \thechapter\ 章 \hspace{4mm} #1}{}}
    \fancyhead[LE,RO]{学习笔记}
    \renewcommand{\figurename}{\textsc{图}}
    \renewcommand\contentsname{目 录}


Notes of Chouqin
=================================

这个项目用于我的学习笔记，包括读书，读代码的一些总结。


.. _cpp:

C/C++
-----------

.. toctree::
    :maxdepth: 2
    
    cpp/tcpl
    cpp/tcpppl
    cpp/practice

.. _python:

Python
-----------
.. toctree::
    :maxdepth: 2
    
    python/basic
    python/faq

.. _linux:

Java
---------------

.. toctree::
    :maxdepth: 2

    java/basic.rst
    java/effjava.rst

Linux
-----------

.. _data-mining:

Data Mining
-----------
.. toctree::
    :maxdepth: 2
    
    data_mining/mining_of_massive_data_set
    data_mining/introduction_to_data_mining
    data_mining/coursera_ml.rst


.. _tools:

Tools
-------------------
.. toctree::
    :maxdepth: 2

    tools/pandoc

Arts of Programming
---------------------
.. toctree::
    :maxdepth: 2

    arts/coders_at_work

.. Indices and tables
.. ==================

.. * :ref:`genindex`
.. * :ref:`modindex`
.. * :ref:`search`
