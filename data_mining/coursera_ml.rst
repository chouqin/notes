Coursera Machine Learning
================================

为什么需要Feature Scaling，因为这样能够收敛更快。

如何选择Linear Regression里面的 :math:`\alpha`?, 画图，
如果不收敛了，减少到0.3倍，如果收敛太慢，可以增加到3倍。

:math:`X^TX` 不可逆，原因有两个：
1. Feature之间线性相关。
2. :math:`m \leq n`

在做Polynomial Regression时，各个维度的Scale不一样，
需要做Feature Scaling.

如果一个学习算法效果不好，有哪些可以改进的地方；
1. 更多的training example.
2. 更少的features
3. 更多的features
4. 调节参数

为什么需要Cross Validation Set？因为如果使用Test Set来选择参数的话，
会使得选中的参数只是对当前的Test Set最好的，
对结果的预测值会偏高。原文是说：

    A new parameter has been fit to the cross validataion set.

通过Learning Curve（准确率和样本数的相关曲线）来定位问题，
确定是增大feature，还是增大数据集。

使用F1来在Precision和Recall之间做一个权衡，
一个好的算法既不能有很低的precision也不能有很低的recall。

SVM的另外一种解释，通过cost函数把logistic regression和SVM联系起来。

Random Initialization的好处，能够确保每次迭代都有变化（在神经网络中），
否则每个Feature都一样的话是始终只有一个Feature, 打破对称性。

如何选择Kmeans的k，使用elbow-method，选取elbow点。

如何选取PCA的特征数，能够保持99% variance的最小的K。
Anomaly Detection相对于Classification来说，如果Positive的sample很少的话可以采用Anomaly Detection。

使用F1来确定 :math:`\epsilon`.

使用 ``hist`` 来看feature是否复合高斯分布，如果不是，
做相应转换，如 :math:`log(x), x^{\frac{1}{2}}, x^{\frac{1}{3}}` 等。

如何生成新的feature，能够把anomaly区分开来。

正协相关和负协相关的判定，根据轴的方向。

什么时候使用original，什么时候multivariate guassian.

使用original:

* 计算量要小很多
* 可以手动添加feature来模拟feature之间的相关性

使用multivariate：

* 计算量大
* 必须保证 :math:`m > n` ，否则 :math:`\Sigma` 不可逆。
* 能够自动捕捉feature之间的相关性。

使用Mean Normalization来解决推荐系统的冷启动问题。

如果检查SGD的converge？每次更新之前计算cost，
取1000次的平均值画曲线，看是否收敛。

在SGD中，使用更小的 :math:`\alpha` 有可能导致更好的收敛结果。

在SGD中，可以随时间增长减少 :math:`\alpha`.

如何synthesis data? 加上背景，旋转，缩放，变形。
而加入random noise没有意义。

使用Ceiling analysis来确定component.
