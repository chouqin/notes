.. Data Mining
.. ==============

.. toctree::
    :maxdepth: 2

    mining_of_massive_data_set
    introduction_to_data_mining
